# Bonobot

If are reading this, you may wondering what I have to sell.

I'll start by the short answer, then elaborate.
___________________________
# I can make you rich. Filthy rich. We're talking seven figures rich ! 
___________________________

So, what follows may be annoyingly long, but you may want to read it anyway. 


## Wow how so ?

There are tons and tons of crypto trading bots you can pay for on the internet.
But ask yourself a question: 

__if those bots are working so well, why on earth people that developed those bot
would need to sell it to you ?__

 They could just make money with it and bothering selling you license would be useless.

We must come to the conclusion: __if there are bot out there, that outperform human traders (and they DO exist), you won't have a 
chance to use it.__

I'm here to give you that chance.


Three questions must be addressed from here:
* How ?
* Why would I sell it, given what I just said ?
* Why would you trust what I'm saying ?

## How ?

I've been working on that project for a little more than two years. 
I'm using cutting edge machine learning.

It works beyond my expectation at forecasting markets.

I'll give plenty of details on the technical, that's not the point
of this current presentation.

## Why would I sell it ?

That's the key aspect of all this, I have a bunch of machine learning models working
together to forecast short, middle and long term price action.

I made money already, I'm very confident it works.

Now, there is a lot of work left. It doesn't exactly trade, I need to work on placing the right
order, with the right sizing. I need to implement stop loss, to figure out the best order scaling patterns.

In short, it's not usable yet, and I want it to, so the idea I had is crowdfunding.

### How would that work exactly ?

I will sell a limited amount of monthly paid licences. What you would gain
being part of it, is, in a first time, daily updates on how it's going, details on
what I'm working on at the moment, and some forecasts.

In itself, not worth a penny but, when it's ready you will have a neat, easy to
use interface. You will have access to every feature, forever.

### OK so, why don't I just wait until it's over and then pay for it ?

I have a very precise amount I need each month.
Consequently, as soon as places are sold out, no one will have this opportunity ever again.

### How much time will it take ?

I have two answers: two months, and six months.
Two months because that's my estimate, if I can be full time on that project.

Six months because, I've you know anything about software development, you know
things don't always go as we want them to. I can spend more time that I first planned to on
implementing even minor parts as communication with a platform API that behave strangely.

Aside from that, I don't want the pressure of a deadline.
Six month is a worst case scenario. If you commit to the adventure, just 
be aware that it SHOULD take two month for you to use the thing but it may take anything from only two
to six. But quite frankly, if it's going to make you rich, doesn't it worth the wait ?


## Why should I trust you ?

Here's a general piece of advice, don't trust some random dude on the internet.
"Random dude on the internet" is probably the less trustable kind of person.

You should put the same amount of trust in a random dude on the internet as in a former child molester
applying to be a nany.


__I'm telling you the truth, I'm legit, this can make you rich__

But again, why would you trust that statement ?

Should I be reading this, in your position, my rational would be that it worth
a shot to invest a small amount of money if the possibility of being rich in return
is even a possibility.


Two more question I want to address:
* What kind of "becoming rich" are we talking about ?
* How much will it cost ?

## What kind of "becoming rich" are we talking about ?

My last projections let me hope the profit, when everything is in place, could be about an average of 2% daily.

It's about what a very, very good day trader is able to do.

If 2% doesn't seem like a lot... well, it counter-intuitive but since it's 2% of the capital each time, look at what it gives
you over a period of one year, assuming $1000 of initial capital.

```
day 1	=>1,000
day 2	=>1,020.0
day 3	=>1,040.4
day 4	=>1,061.21
day 5	=>1,082.43
day 6	=>1,104.08
day 7	=>1,126.16
day 8	=>1,148.69
day 9	=>1,171.66
day 10	=>1,195.09
day 11	=>1,218.99
day 12	=>1,243.37
day 13	=>1,268.24
day 14	=>1,293.61
day 15	=>1,319.48
day 16	=>1,345.87
day 17	=>1,372.79
day 18	=>1,400.24
day 19	=>1,428.25
day 20	=>1,456.81
day 21	=>1,485.95
day 22	=>1,515.67
day 23	=>1,545.98
day 24	=>1,576.9
day 25	=>1,608.44
day 26	=>1,640.61
day 27	=>1,673.42
day 28	=>1,706.89
day 29	=>1,741.02
day 30	=>1,775.84
day 31	=>1,811.36
day 32	=>1,847.59
day 33	=>1,884.54
day 34	=>1,922.23
day 35	=>1,960.68
day 36	=>1,999.89
day 37	=>2,039.89
day 38	=>2,080.69
day 39	=>2,122.3
day 40	=>2,164.74
day 41	=>2,208.04
day 42	=>2,252.2
day 43	=>2,297.24
day 44	=>2,343.19
day 45	=>2,390.05
day 46	=>2,437.85
day 47	=>2,486.61
day 48	=>2,536.34
day 49	=>2,587.07
day 50	=>2,638.81
day 51	=>2,691.59
day 52	=>2,745.42
day 53	=>2,800.33
day 54	=>2,856.33
day 55	=>2,913.46
day 56	=>2,971.73
day 57	=>3,031.17
day 58	=>3,091.79
day 59	=>3,153.62
day 60	=>3,216.7
day 61	=>3,281.03
day 62	=>3,346.65
day 63	=>3,413.58
day 64	=>3,481.86
day 65	=>3,551.49
day 66	=>3,622.52
day 67	=>3,694.97
day 68	=>3,768.87
day 69	=>3,844.25
day 70	=>3,921.14
day 71	=>3,999.56
day 72	=>4,079.55
day 73	=>4,161.14
day 74	=>4,244.36
day 75	=>4,329.25
day 76	=>4,415.84
day 77	=>4,504.15
day 78	=>4,594.24
day 79	=>4,686.12
day 80	=>4,779.84
day 81	=>4,875.44
day 82	=>4,972.95
day 83	=>5,072.41
day 84	=>5,173.86
day 85	=>5,277.33
day 86	=>5,382.88
day 87	=>5,490.54
day 88	=>5,600.35
day 89	=>5,712.35
day 90	=>5,826.6
day 91	=>5,943.13
day 92	=>6,062.0
day 93	=>6,183.24
day 94	=>6,306.9
day 95	=>6,433.04
day 96	=>6,561.7
day 97	=>6,692.93
day 98	=>6,826.79
day 99	=>6,963.33
day 100=>7,102.59
day 101=>7,244.65
day 102=>7,389.54
day 103=>7,537.33
day 104=>7,688.08
day 105=>7,841.84
day 106=>7,998.67
day 107=>8,158.65
day 108=>8,321.82
day 109=>8,488.26
day 110=>8,658.02
day 111=>8,831.18
day 112=>9,007.81
day 113=>9,187.96
day 114=>9,371.72
day 115=>9,559.16
day 116=>9,750.34
day 117=>9,945.35
day 118=>10,144.25
day 119=>10,347.14
day 120=>10,554.08
day 121=>10,765.16
day 122=>10,980.47
day 123=>11,200.08
day 124=>11,424.08
day 125=>11,652.56
day 126=>11,885.61
day 127=>12,123.32
day 128=>12,365.79
day 129=>12,613.1
day 130=>12,865.37
day 131=>13,122.67
day 132=>13,385.13
day 133=>13,652.83
day 134=>13,925.89
day 135=>14,204.4
day 136=>14,488.49
day 137=>14,778.26
day 138=>15,073.83
day 139=>15,375.3
day 140=>15,682.81
day 141=>15,996.47
day 142=>16,316.4
day 143=>16,642.72
day 144=>16,975.58
day 145=>17,315.09
day 146=>17,661.39
day 147=>18,014.62
day 148=>18,374.91
day 149=>18,742.41
day 150=>19,117.26
day 151=>19,499.6
day 152=>19,889.59
day 153=>20,287.39
day 154=>20,693.13
day 155=>21,107.0
day 156=>21,529.14
day 157=>21,959.72
day 158=>22,398.91
day 159=>22,846.89
day 160=>23,303.83
day 161=>23,769.91
day 162=>24,245.31
day 163=>24,730.21
day 164=>25,224.82
day 165=>25,729.31
day 166=>26,243.9
day 167=>26,768.78
day 168=>27,304.15
day 169=>27,850.23
day 170=>28,407.24
day 171=>28,975.38
day 172=>29,554.89
day 173=>30,145.99
day 174=>30,748.91
day 175=>31,363.89
day 176=>31,991.17
day 177=>32,630.99
day 178=>33,283.61
day 179=>33,949.28
day 180=>34,628.27
day 181=>35,320.83
day 182=>36,027.25
day 183=>36,747.79
day 184=>37,482.75
day 185=>38,232.4
day 186=>38,997.05
day 187=>39,776.99
day 188=>40,572.53
day 189=>41,383.98
day 190=>42,211.66
day 191=>43,055.9
day 192=>43,917.01
day 193=>44,795.35
day 194=>45,691.26
day 195=>46,605.09
day 196=>47,537.19
day 197=>48,487.93
day 198=>49,457.69
day 199=>50,446.84
day 200=>51,455.78
day 201=>52,484.9
day 202=>53,534.6
day 203=>54,605.29
day 204=>55,697.39
day 205=>56,811.34
day 206=>57,947.57
day 207=>59,106.52
day 208=>60,288.65
day 209=>61,494.42
day 210=>62,724.31
day 211=>63,978.8
day 212=>65,258.37
day 213=>66,563.54
day 214=>67,894.81
day 215=>69,252.71
day 216=>70,637.76
day 217=>72,050.52
day 218=>73,491.53
day 219=>74,961.36
day 220=>76,460.58
day 221=>77,989.8
day 222=>79,549.59
day 223=>81,140.58
day 224=>82,763.4
day 225=>84,418.66
day 226=>86,107.04
day 227=>87,829.18
day 228=>89,585.76
day 229=>91,377.48
day 230=>93,205.03
day 231=>95,069.13
day 232=>96,970.51
day 233=>98,909.92
day 234=>100,888.12
day 235=>102,905.88
day 236=>104,964.0
day 237=>107,063.28
day 238=>109,204.54
day 239=>111,388.63
day 240=>113,616.41
day 241=>115,888.74
day 242=>118,206.51
day 243=>120,570.64
day 244=>122,982.05
day 245=>125,441.69
day 246=>127,950.53
day 247=>130,509.54
day 248=>133,119.73
day 249=>135,782.12
day 250=>138,497.77
day 251=>141,267.72
day 252=>144,093.08
day 253=>146,974.94
day 254=>149,914.44
day 255=>152,912.72
day 256=>155,970.98
day 257=>159,090.4
day 258=>162,272.21
day 259=>165,517.65
day 260=>168,828.0
day 261=>172,204.56
day 262=>175,648.66
day 263=>179,161.63
day 264=>182,744.86
day 265=>186,399.76
day 266=>190,127.75
day 267=>193,930.31
day 268=>197,808.91
day 269=>201,765.09
day 270=>205,800.39
day 271=>209,916.4
day 272=>214,114.73
day 273=>218,397.03
day 274=>222,764.97
day 275=>227,220.27
day 276=>231,764.67
day 277=>236,399.96
day 278=>241,127.96
day 279=>245,950.52
day 280=>250,869.53
day 281=>255,886.92
day 282=>261,004.66
day 283=>266,224.76
day 284=>271,549.25
day 285=>276,980.24
day 286=>282,519.84
day 287=>288,170.24
day 288=>293,933.64
day 289=>299,812.31
day 290=>305,808.56
day 291=>311,924.73
day 292=>318,163.23
day 293=>324,526.49
day 294=>331,017.02
day 295=>337,637.36
day 296=>344,390.11
day 297=>351,277.91
day 298=>358,303.47
day 299=>365,469.54
day 300=>372,778.93
day 301=>380,234.51
day 302=>387,839.2
day 303=>395,595.98
day 304=>403,507.9
day 305=>411,578.06
day 306=>419,809.62
day 307=>428,205.81
day 308=>436,769.93
day 309=>445,505.33
day 310=>454,415.43
day 311=>463,503.74
day 312=>472,773.82
day 313=>482,229.29
day 314=>491,873.88
day 315=>501,711.36
day 316=>511,745.59
day 317=>521,980.5
day 318=>532,420.11
day 319=>543,068.51
day 320=>553,929.88
day 321=>565,008.48
day 322=>576,308.65
day 323=>587,834.82
day 324=>599,591.52
day 325=>611,583.35
day 326=>623,815.01
day 327=>636,291.31
day 328=>649,017.14
day 329=>661,997.48
day 330=>675,237.43
day 331=>688,742.18
day 332=>702,517.02
day 333=>716,567.36
day 334=>730,898.71
day 335=>745,516.69
day 336=>760,427.02
day 337=>775,635.56
day 338=>791,148.27
day 339=>806,971.24
day 340=>823,110.66
day 341=>839,572.88
day 342=>856,364.33
day 343=>873,491.62
day 344=>890,961.45
day 345=>908,780.68
day 346=>926,956.29
day 347=>945,495.42
day 348=>964,405.33
day 349=>983,693.44
day 350=>1,003,367.3
day 351=>1,023,434.65
day 352=>1,043,903.34
day 353=>1,064,781.41
day 354=>1,086,077.04
day 355=>1,107,798.58
day 356=>1,129,954.55
day 357=>1,152,553.64
day 358=>1,175,604.71
day 359=>1,199,116.81
day 360=>1,223,099.14
day 361=>1,247,561.13
day 362=>1,272,512.35
day 363=>1,297,962.6
day 364=>1,323,921.85
```

Yep... 2% a day is x1000 per year ! 


## How much will it cost ?

I don't have an answer. In fact, YOU are going to give me that answer ! 

The equation is simple: I want about $9,000 monthly (I need to work that out more precisely, that's a rough estimate).
But first, let me break this down:
* I want $3000 for living. (My girlfriend fault :), we negociated that she's up for me quiting my job for this project, at the condition I earn the exact same thing as in my current job)

* I want to pay a dev for the web interface you guys will be using

* It would tremendously help to have a lot of cloud computing power for training all the machine learning models

* Crowdfunding implies I create some sort of company, and there will be taxes...


### How come YOU decide how much it will cost ?

Assuming this 9k figure (which will be refined soon), I will want that, not more, and therefore the amount of place will be limited by that factor only.

Now, it can be anywhere from 9000 folks paying $1 monthly to two people paying $4500.


So, I ask you, in the #How-much discord channel, to post how much would you agree to pay __AT MOST__ for that (given the conditions I explained). As soon as it represent enough, I will take the minumum and that will be the price.


I'm not sure this is super clear so here's an example:
```
random dude1 post: 5000
random dude2 post: 3000
random dude3 post: 2000
random dude4 post: 2000
random girl1 post: 1500
random girl2 post: 1800
```

In this example, everyone is ok to pay $1500 or more.
1500x6 = 9000 (yeah, I'm pretty good with numbers) therefore, every one will be ask for 1500 monthly.

(Obviously, this was only for the sake of illustration, I expect more something in the two digits territory...)

## What will it look like when ready ?

It is going to be a simple and beautiful web interface (hence the need for a freelance dev, I'm bad at beautiful web interfaces). You will be ask to give an API key of the platform of your choice, tell the bot the assets you want it to trade for you and voila !

## Will there be a limit for the capital I can trade ?

I may be a bit paranoid, but to avoid a sneaky whale or bank to abuse the thing, then sue my ass (because I assume that's the kind of thing rich folks do), the capital traded will be capped to one million USD. 
Now, if you have that kind of capital to trade, DM me, we can certainly work something out :)
